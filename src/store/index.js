import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'
import { pause, generateComment } from '../data/helpers'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        messagesList: []
    },
    getters: {
        getField,
        getMessageObj: state => id => {
            return state.messagesList.find(message => {
                return message.messageId === id
            })
        },
        getLastMessages (state) {
            const newMessagesList = state.messagesList.sort((a, b) => {
                return b.comments.length - a.comments.length
            })
            return newMessagesList.slice(0, 3)
        }
    },
    mutations: {
        updateField,
        setMessage (state, model) {
            state.messagesList.unshift(model)
        },
        setComment (state, payload) {
            const index = state.messagesList.findIndex(message => {
                return message.messageId === payload.messageId
            })
            if (index !== -1) {
                state.messagesList[index].comments.unshift(payload.comment)
            }
        }
    },
    actions: {
        async sendMessage ({ commit }, model) {
            await pause(300)
            commit('setMessage', model)
        },
        generateComment ({ commit }, messageId) {
            commit('setComment', {
                comment: generateComment(),
                messageId
            })
        }
    },
    modules: {
    }
})
