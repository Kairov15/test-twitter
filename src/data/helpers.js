import { LoremIpsum } from 'lorem-ipsum'
import { commentsModel } from './model'

const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4
    },
    wordsPerSentence: {
        max: 16,
        min: 4
    }
})

function getRandomNumber (min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function getRandomMs () {
    return getRandomNumber(5, 30) * 1000
}

function generateText (min, max) {
    const wordsCount = getRandomNumber(min, max)
    return lorem.generateWords(wordsCount)
}

function generateId () {
    return `_${Date.now().toString(36)}${Math.random().toString(36).substr(2, 5)}`
}

async function pause (ms) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

function generateComment () {
    const model = { ...commentsModel }
    model.comment = generateText(1, 20)
    model.commentId = generateId()
    model.userName = `User-${generateText(1, 2)}`
    model.img = 'https://picsum.photos/100/100'
    return model
}

export { getRandomMs, generateText, generateId, pause, generateComment }
