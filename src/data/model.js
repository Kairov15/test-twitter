const messageModel = {
    messageId: null,
    date: null,
    message: null,
    comments: []
}

const commentsModel = {
    commentId: null,
    img: null,
    comment: null,
    userName: null
}

export { messageModel, commentsModel }
